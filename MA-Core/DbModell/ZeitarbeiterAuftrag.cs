﻿using System;
namespace MA_Core.DbModell
{
    public class ZeitarbeiterAuftrag
    {
        public int Id { get; set; }

        public Zeitarbeiter Zeitarbeiter { get; set; }
        public Auftrag Auftrag { get; set; }
    }
}
