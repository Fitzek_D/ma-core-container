import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { LoginDto } from '../interfaces/LoginDto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    message: string;
    anmeldeForm: FormGroup;
    
    constructor(public authService: AuthService, public router: Router, private formBuilder: FormBuilder) { 
      this.message = this.getMessage();
    }
  
    public ngOnInit() {
      this.anmeldeForm = this.formBuilder.group({
        acc: ['', Validators.required],
        pw: ['', Validators.required],
      });
    }
  
    getMessage() {
      return (this.authService.isLoggedIn ? 'An' : 'Ab') + 'gemeldet ' ;
    }
  
    submit() {
        this.message = 'Anmeldung ...';
    
        const acc = this.anmeldeForm.get('acc').value;
        const pw  = this.anmeldeForm.get('pw').value;

        const loginData: LoginDto = {
            name: acc,
            pw: pw
        };
        
        this.authService.login(loginData);
        
        if (this.authService.isLoggedIn) {
            this.router.navigate(['']);
        }
    }
  
    logout() {
      this.authService.logout();
      this.message = this.getMessage();
    }
  }
  

