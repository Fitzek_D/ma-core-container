import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { AccountDto } from '../interfaces/AccountDto';
import { KundeDto } from '../interfaces/KundeDto';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  user$: Observable<AccountDto>;

  benutzerForm: FormGroup;

  constructor(public authService: AuthService, private formBuilder: FormBuilder, private http: HttpClient) {  }

  public ngOnInit() {
    this.user$ = this.authService.userSubject;

    this.benutzerForm = this.formBuilder.group({
      acc: ['', Validators.required],
      pw: ['', Validators.required],
      name: ['', Validators.required],
      role: ['', Validators.required]
    });
  }
  submit(kunde: string) {

    const acc = this.benutzerForm.get('acc').value;
    const pw  = this.benutzerForm.get('pw').value;
    const name  = this.benutzerForm.get('name').value;
    const role  = this.benutzerForm.get('role').value;

    const neuerKunde: KundeDto = {
        name: name,
        passwort: pw,
        benutzername: acc,
        rolle: role,
        kundenname: kunde
    };

    this.http.post('api/benutzer/add-benutzer', neuerKunde).subscribe(erg => {
      console.log(erg);
    })

    
    
  }
}
