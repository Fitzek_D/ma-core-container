﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_Core.Db;
using MA_Core.DbModell;
using MA_Core.Dto;
using Microsoft.AspNetCore.Mvc;


namespace MA_Core.Controllers
{
    [Route("api/[controller]/[action]")]
    public class LogInController : Controller
    {
        [HttpPost]
        [ActionName("check-login")]
        public ActionResult<AccountDto> CheckLogin([FromBody] LogInDto login, [FromServices] Database db)
        {
            var account = new AccountDto();

            var benutzers = db.Benutzer.ToList();

            Benutzer benutzer = db.Benutzer.SingleOrDefault(b => b.Benutzername == login.Name);

            if(benutzer.KundeId != null)
            {
            }

            if (benutzer != null)
            {
                if (benutzer.Passwort == login.Pw)
                {
                    account.ValidLogIn = true;
                    account.Benutzername = benutzer.Name;
                    account.Rolle = benutzer.Rolle;
                    account.Id = benutzer.Id;

                    switch (benutzer.Rolle)
                    {
                        case "Entwickler":
                            break;
                        case "Benutzer":
                            var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

                            account.Kundenname = kunde.Name;

                            var kundenproudkte = db.KundenProdukte.Where(kp => kp.Kunde.Id == kunde.Id).ToList();

                            var produkte = db.Produkte.ToList();

                            var produktliste = new List<ProduktDto>();

                            foreach (var produkt in kundenproudkte)
                            {
                                var dbProdukt = db.Produkte.SingleOrDefault(p => p.Id == produkt.Produkt.Id);

                                // -container hinzufügen zu den links
                                var temp = dbProdukt.Link.Split(".", 2);
                                var containerTempLink = temp[0] + "-container." + temp[1];

                                var containerLink = containerTempLink.Replace("-df", "");

                                var produktDto = new ProduktDto
                                {
                                    Produktname = dbProdukt.Name,
                                    Produktlink = containerLink
                                };
                                produktliste.Add(produktDto);
                            }

                            account.Produkte = produktliste;

                            break;
                        case "Admin":
                            var kunde2 = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

                            account.Kundenname = kunde2.Name;

                            var kundenproudkte2 = db.KundenProdukte.Where(kp => kp.Kunde.Id == kunde2.Id).ToList();

                            var produkte2 = db.Produkte.ToList();

                            var produktliste2 = new List<ProduktDto>();

                            foreach (var produkt in kundenproudkte2)
                            {
                                var dbProdukt = db.Produkte.SingleOrDefault(p => p.Id == produkt.Produkt.Id);

                                // -container hinzufügen zu den links https://ma-csv-df.azurewebsites.net
                                var temp = dbProdukt.Link.Split(".", 2);
                                var containerTempLink = temp[0] + "-container." + temp[1];

                                var containerLink = containerTempLink.Replace("-df", "");

                                var produktDto = new ProduktDto
                                {
                                    Produktname = dbProdukt.Name,
                                    Produktlink = containerLink
                                };
                                produktliste2.Add(produktDto);
                            }

                            account.Produkte = produktliste2;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    account.ValidLogIn = false;
                }
            }
            else
            {
                account.ValidLogIn = false;
            }

            return Ok(account);
        }

    }

}
