import { ProduktDto } from "./ProduktDto";

export interface AccountDto {
    validLogIn: boolean;
    benutzername: string;
    kundenname: string;
    rolle: string;
    produkte: ProduktDto[];
    id: number;
}