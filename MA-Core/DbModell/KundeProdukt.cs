﻿using System;
namespace MA_Core.DbModell
{
    public class KundeProdukt
    {
        public int Id { get; set; }

        public Kunde Kunde { get; set; }
        public Produkt Produkt { get; set; }
    }


}
