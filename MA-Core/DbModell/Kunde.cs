﻿using System;
using System.Collections.Generic;

namespace MA_Core.DbModell
{
    public class Kunde
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Benutzer> Benutzer { get; set; }
        public virtual ICollection<KundeProdukt> Produkte { get; set; }
        public ICollection<Auftrag> Auftraege { get; set; }
        public ICollection<Zeitarbeiter> Zeitarbeiter { get; set; }
    }

}
